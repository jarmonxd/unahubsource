-- Variable
local Client = game.Players.LocalPlayer
local rq = request

-- Function
local function sendwebhook()
    local map = "PlaceId : ```" .. tostring(game.PlaceId) .. "```"
    local text = "UserID : ```" .. tostring(Client.UserId) .. "``` User Name : ```" .. Client.Name .. "```"
    local webhook = "https://discord.com/api/webhooks/1235895159472918620/JmE4Cxvz74BKBZm77MC4Q3jSB2XxjoHByjwca3bmP6HeYNF7D3WHHZPWip4mtsaBw3Dk"
    local Time = os.date("%d/%m/%Y %H:%M:%S")
    local hwid = "hwid : ```" .. (game:GetService("RbxAnalyticsService"):GetClientId()) .. "``` Executer ? ```"..getexecutorname().."```"
    rq({
        Url = webhook,
        Method = "POST",
        Headers = {
            ["Content-Type"] = "application/json"
        },
        Body = game:GetService("HttpService"):JSONEncode({
            ["content"] = "<@&1232552008289095732>",
            ["embeds"] = {
                {
                ["author"] = {
                        ["name"] = "เช็คการรันสคริปต์ 📜",
                },
                ["fields"] = {
                    {
                        ["name"] = "Player Info :sparkles:",
                        ["value"] = text,
                        ['inline'] = false
                    },
                    {
                        ["name"] = "Game :video_game:",
                        ["value"] = map,
                        ['inline'] = false
                    },
                    {
                        ["name"] = "hwid player :pencil:",
                        ["value"] = hwid,
                        ['inline'] = false
                    },
                    {
                        ["name"] = "Time :alarm_clock:",
                        ["value"] = Time,
                        ['inline'] = false
                    },
                },
                ["color"] = tonumber(16777215),
                ["footer"] = {["text"] = "Una Hub", ["icon_url"] = "https://pic.puttipong-studio.in.th/secure-storage/141874068220240403_210211.png"}
                }
             }
        })
    })
end

print(os.date())
spawn(sendwebhook)